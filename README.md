#CSV to Scenario Outline table (for Behat)

This script takes a CSV file, parse it and generates 
a Scenario Outline table that can then be used in your 
Behat feature file.

## Usage

```
Usage: php outliner.php -i=(FILENAME) [-o=(FILENAME)]

    -i [--input]  FILENAME  Specify an input file containing the points to be processed
    -o [--output] FILENAME  Specify an output file where to write the post processing result
    -h [--help]             You're looking at it
    -v [--version]          Show version


```