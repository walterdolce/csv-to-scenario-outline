#!/usr/bin/env php
<?php

ini_set('auto_detect_line_endings', true);

if (PHP_VERSION_ID < 50400) {
    fwrite(STDERR, sprintf(
        "Minimum PHP version required to run this script is 5.4.0. Current version is %s.",
        phpversion()
    ));
    exit(1);
}

$version = 'v0.1';

$usage = <<<USAGE

Usage: php outliner.php -i=(FILENAME) [-o=(FILENAME)]

    -i [--input]  FILENAME  Specify an input file containing the points to be processed
    -o [--output] FILENAME  Specify an output file where to write the post processing result
    -h [--help]             You're looking at it
    -v [--version]          Show version


USAGE;

$options = getopt('i:o:hv', [
    'input:',
    'output:',
    'help',
    'version'
]);

if (empty($options)) {
    fwrite(STDERR, "No options specified." . PHP_EOL. $usage);
    exit(1);
}

foreach (array_keys($options) as $option) {
    switch ($option) {
        case 'i':
        case 'input':
            $inputFile  = trim($options[$option]);
            $outputFile = pathinfo($inputFile)['filename'] . '.feature';
            break;
        case 'o':
        case 'output':
            $outputFile = trim($options[$option]);
            break;
        case 'h':
        case 'help':
            fwrite(STDOUT, $usage);
            exit(0);
            break;
        case 'v':
        case 'version':
            fwrite(STDOUT, $version);
            exit(0);
            break;
    }
}

if (empty($inputFile)) {
    fwrite(STDERR, 'No valid filename provided. Nothing to do.');
    exit(1);
}

if (!is_file($inputFile) || !is_readable($inputFile)) {
    fwrite(STDERR, "File '$file' is not a file or it's not readable. Nothing to parse.");
    exit(1);
}

$matrix = [];

$lines = file($inputFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
foreach ($lines as $line) {
    $matrix[] = str_getcsv($line);
}

$matrix = transpose($matrix);

foreach ($matrix as $index => $column) {
    $lengths   = array_map('strlen', $column);
    $maxLength = max($lengths);

    foreach ($column as $row => $value) {
        $column[$row] = str_pad($value, $maxLength, ' ');
    }

    $matrix[$index] = $column;
}

$lines  = [];

$matrix = transpose($matrix);
foreach ($matrix as $index => $row) {
    $lines[] = '| ' . implode(' | ', $row) . ' |';
}

$content = implode(PHP_EOL, $lines);

file_put_contents($outputFile, $content);

echo 'Done.';
exit;

function transpose($array)
{
    $transposed_array = [];
    if ($array) {
        foreach ($array as $row_key => $row) {
            if (is_array($row) && !empty($row)) {
                foreach ($row as $column_key => $element) {
                    $transposed_array[$column_key][$row_key] = $element;
                }
            } else {
                $transposed_array[0][$row_key] = $row;
            }
        }
    }

    return $transposed_array;
}
